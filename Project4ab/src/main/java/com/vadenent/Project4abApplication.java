package com.vadenent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Project4abApplication {

	public static void main(String[] args) {
		SpringApplication.run(Project4abApplication.class, args);
	}

}
