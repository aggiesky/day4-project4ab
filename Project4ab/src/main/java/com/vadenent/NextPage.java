package com.vadenent;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Component
public class NextPage {

	private String nextPage;

	public NextPage() {
		nextPage = "InitialPage";
	}

	public String getNextPage() {
		return this.nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}
}
