package com.vadenent;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Component
public class CoinFlipper {

	private Integer totalNumberOfFlips;
	private Integer totalNumberOfHeads;
	private Integer totalNumberOfTails;

	private Integer totalNumberOfWins;

	private String previousGuess;
	private String previousFlipResult;
	private Boolean previousGuessAWinner;

	private Integer totalWinningPercentage;
	private String bannerForPreviousGame;

	// Initialize cf upon instantiation as per reset()
	public CoinFlipper() {
		reset();
	}

	// Getters needed to support view
	public String getBannerForPreviousGame() {
		return this.bannerForPreviousGame;
	}

	public String getPreviousGuess() {
		return this.previousGuess;
	}

	public String getPreviousFlipResult() {
		return this.previousFlipResult;
	}

	public Integer getTotalNumberOfFlips() {
		return this.totalNumberOfFlips;
	}

	public Integer getTotalWinningPercentage() {
		return this.totalWinningPercentage;
	}

	public Integer getTotalNumberOfHeads() {
		return this.totalNumberOfHeads;
	}

	public Integer getTotalNumberOfTails() {
		return this.totalNumberOfTails;
	}

	// Initialize/reset property values
	public void reset() {

		// Set initial values for the session scope attributes

		this.totalNumberOfFlips = 0;
		this.totalNumberOfHeads = 0;
		this.totalNumberOfTails = 0;
		this.totalNumberOfWins = 0;
		this.totalWinningPercentage = 0;

		this.previousGuess = "Heads"; // value just needs to not be NULL
		this.previousFlipResult = "Heads"; // value just needs to not be NULL
		this.bannerForPreviousGame = "Good Luck!"; // value just needs to not be NULL
		this.previousGuessAWinner = true; // value just needs to not be NULL

	}

	// previousGuess is set from the request parameter "userGuess"
	public void setPreviousGuess(String userGuess) {
		this.previousGuess = userGuess;
	}

	// This is where all of the work is done.
	// Pseudo-random number between 0 and 1 inclusive is generated for the flip
	// results.
	// The other properties are set accordingly.

	public void flipTheCoin() {

		// flip the coin: 0 is Tails; 1 is Heads
		// add new flip results to the totals

		Integer flipResult = ThreadLocalRandom.current().nextInt(0, 2);
		
		if (flipResult.intValue() == 0) 	previousFlipResult = "Tails";
		if (flipResult.intValue() == 1)		previousFlipResult = "Heads";

		this.totalNumberOfHeads += flipResult;
		this.totalNumberOfFlips++;
		this.totalNumberOfTails = this.totalNumberOfFlips - this.totalNumberOfHeads;

		// Determine if the guess was right (i.e. was it a winner?); previousGuessAWinner is Boolean
		this.previousGuessAWinner = 
				   (this.previousGuess.contentEquals("Heads") && flipResult.equals(Integer.valueOf(1)))		// guessed heads & result is heads
				|| (this.previousGuess.contentEquals("Tails") && flipResult.equals(Integer.valueOf(0)));	// guessed tails & result is tails

		// If previous guess was a winner, tally a win;
		// Set the result message to display to the user
		if (this.previousGuessAWinner) {
			this.totalNumberOfWins += 1;
			this.bannerForPreviousGame = "You Win!";
		} else {
			this.bannerForPreviousGame = "You Lose!";
		}

		// Forcing to be an integer showing the % without any fraction
		this.totalWinningPercentage = (100 * this.totalNumberOfWins) / this.totalNumberOfFlips;

	}

}
