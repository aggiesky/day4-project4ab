package com.vadenent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/CoinToss")
public class HeadsOrTails {

	@Autowired
	private CoinFlipper coinFlipper;

	@Autowired
	private NextPage nextPage;

	@GetMapping
	public String welcomeHandler(Model model) {

		
		model.addAttribute("coinFlipper", coinFlipper);
		model.addAttribute("nextPage", nextPage.getNextPage());

		return "InitialPage";
	}

	@PostMapping("/main")
	public String flipIt(Model model, @RequestParam("userGuess") String userGuess) {

		model.addAttribute("coinFlipper", coinFlipper);
		model.addAttribute("nextPage", nextPage.getNextPage());

		// userGuess can be {"Heads", "Tails", "Reset", "Restart", "EndGame"}
		// it tells us what action should be taken
		switch (userGuess) {

		// The user has guessed a flip result of one of "Heads" or "Tails", so flip the coin and see
		case "Heads":
		case "Tails":
			coinFlipper.setPreviousGuess(userGuess);
			coinFlipper.flipTheCoin();
			nextPage.setNextPage("MainPage");
			break;

		// The user has requested to reset the current game, or to restart after ending the game.
		case "Reset":
		case "Restart":
			coinFlipper.reset();
			nextPage.setNextPage("InitialPage");
			break;

		// The user has request to end the game.
		case "EndGame":
			nextPage.setNextPage("EndingPage");
			break;

		default:

		}

		return "redirect:/CoinToss";
	}

}
